// Función para llamar a la API cuando se hace clic en el botón
document.getElementById("btnPredict").addEventListener("click", function () {
//Datos hardcodeados para probar la API
const data = [1, 2, 3, 4, 5];

  // Realizar una solicitud POST a tu API
  fetch("/predecir/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((data) => {
      // Guarda la respuesta de la API
      const imageBase64 = data.imagen_tabla_base64;

      // Muestra la imagen de la tabla
      const imgElement = document.createElement("img");
      imgElement.src = `data:image/png;base64, ${imageBase64}`;
      document.getElementById("result").appendChild(imgElement);
    })
    .catch((error) => {
      console.error("Error al llamar a la API:", error);
    });
});
