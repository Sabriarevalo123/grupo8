# Importamos librerias
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from plotnine import ggplot, aes, geom_line, geom_point, geom_label, labs, theme_minimal

# Para K-Modes
from kmodes.kmodes import KModes
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import silhouette_score
from sklearn.manifold import TSNE


# Importamos base de datos y corroboramos
egresosHospitalarios = "./resources/egresos-2016-2020.csv"
data = pd.read_csv(egresosHospitalarios, sep=",")
data.head()

# Codificamos las columnas de tipo categórico
edad_encoded = data["grupo_edad"].replace(
    [
        "0-4",
        "5-9",
        "10-14",
        "15-19",
        "20-24",
        "25-29",
        "30-34",
        "35-39",
        "40-44",
        "45-49",
        "50-54",
        "55-59",
        "60-64",
        "65-69",
        "70-74",
        "75-79",
        "80 y mas",
    ],
    [2, 7, 12, 17, 22, 27, 32, 37, 42, 47, 52, 57, 62, 67, 72, 77, 82],
)

# Crear una instancia de OneHotEncoder
encoder = OneHotEncoder()

# Recuperamos causa de egreso
c_egreso = data["causa_egreso"]
c_egreso_reshape = c_egreso.values.reshape(-1, 1)

# Ajustar y transformar los datos
c_egreso_encoded = encoder.fit_transform(data[["causa_egreso"]])
c_egreso_encoded_df = pd.DataFrame(
    c_egreso_encoded.toarray(), columns=encoder.get_feature_names_out(["causa_egreso"])
)

data_encoded = pd.concat([edad_encoded, c_egreso_encoded_df], axis=1)

# Aplicamos K-Modes clustering
km = KModes(n_clusters=19, init="Huang", n_init=2, verbose=1, random_state=42)
cluster_labels = km.fit_predict(data_encoded)

# Agregamos los resultados del clustering al DataFrame
data["cluster"] = cluster_labels

# Observamos el costo
#print(km.cost_)

# Exportar el DataFrame 'data' a un archivo CSV
#data.to_csv("data.csv", index=False)

# Reducción de dimensionalidad con t-SNE
tsne = TSNE(n_components=2, random_state=42)
data_tsne = tsne.fit_transform(data_encoded)

# Reducción de dimensionalidad con t-SNE
tsne = TSNE(n_components=2, random_state=42)
data_tsne = tsne.fit_transform(data_encoded)

# Crear el gráfico de celdas de Voronoi
plt.figure(figsize=(10, 6))

# Marcar los puntos de datos en el gráfico con colores de clúster
plt.scatter(data_tsne[:, 0], data_tsne[:, 1], c=cluster_labels, cmap='viridis', s=20, alpha=0.7)

plt.xlabel('Dimensión 1 (t-SNE)')
plt.ylabel('Dimensión 2 (t-SNE)')
plt.title('Clustering K-Modes Visualizado con t-SNE')
plt.colorbar()

plt.show()

# Crear un DataFrame que combine la edad codificada, las causas de egreso y las etiquetas de cluster
data_clustered = pd.concat([edad_encoded, c_egreso_encoded_df, pd.Series(cluster_labels, name='cluster')], axis=1)

# Calcular la distribución de las causas de egreso en cada grupo de edad para cada cluster
clustered_grouped = data_clustered.groupby(['cluster', 'grupo_edad']).sum().reset_index()

# Crear un gráfico de barras apiladas para mostrar la distribución de las causas de egreso en cada grupo de edad
fig, ax = plt.subplots(figsize=(12, 6))

# Definir los 19 colores que deseas utilizar
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
          'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan',
          'steelblue', 'darkorange', 'limegreen', 'firebrick', 'mediumorchid',
          'sienna', 'hotpink', 'dimgray', 'goldenrod']

for i, cluster in enumerate(clustered_grouped['cluster'].unique()):
    cluster_data = clustered_grouped[clustered_grouped['cluster'] == cluster]
    ax.bar(cluster_data['grupo_edad'], cluster_data.iloc[:, 1:-1].sum(axis=1), label=f'Cluster {cluster}', color=colors[i])

ax.set_title('Distribución de Causas de Egreso en Grupos de Edad')
ax.set_xlabel('Grupo de Edad')
ax.set_ylabel('Cantidad')
ax.legend()
plt.xticks(rotation=45)
plt.tight_layout()
plt.show()

# ## Análisis de los resultados##
# # Calcular el coeficiente de silueta
# silhouette_avg = silhouette_score(data_encoded, cluster_labels)
# print(f"El coeficiente de silueta promedio es: {silhouette_avg}")

# # Crearmos un DataFrame con el resultado del costo de cada cluster
# costo = {'Cluster': [3, 7, 11, 15, 19],
#         'Cost': [1376026.0, 1186363.0, 1072589.0, 989223.0, 939481.0]}
# df_cost = pd.DataFrame(costo)

# # Generar el gráfico
# (
#     ggplot(costo=df_cost) +
#     geom_line(aes(x='Cluster', y='Cost')) +
#     geom_point(aes(x='Cluster', y='Cost')) +
#     geom_label(aes(x='Cluster', y='Cost', label='Cluster'), size=10, nudge_y=1000) +
#     labs(title='Número óptimo de clusters con el método del codo', x='Número de Clusters k', y='Costo') +
#     theme_minimal()
# )