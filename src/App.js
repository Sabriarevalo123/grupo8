import "./App.css";

import Menu from "./Menu";
import Navbar from "./Navbar";
function App() {
  return (
    <div className="App">
      <Navbar />
      <header className="App-header">
        <Menu />
      </header>
    </div>
  );
}

export default App;
