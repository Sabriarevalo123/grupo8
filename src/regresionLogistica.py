# Importamos librerias
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')  #backend con GUI, también puede ser Qt5Agg
import matplotlib.pyplot as plt
import pickle

# Para Regresión Logistica
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (
    mean_squared_error,
    mean_absolute_error,
    accuracy_score,
    roc_curve,
    auc,
)
from xgboost import XGBClassifier

# Importamos base de datos y corroboramos
egresosHospitalarios = "./resources/egresos-2016-2020.csv"
data = pd.read_csv(egresosHospitalarios, sep=",")
data.head()

# Codificamos las columnas de tipo categórico
data_encoded = data
data_encoded["grupo_edad"] = data_encoded["grupo_edad"].replace(
    [
        "0-4",
        "5-9",
        "10-14",
        "15-19",
        "20-24",
        "25-29",
        "30-34",
        "35-39",
        "40-44",
        "45-49",
        "50-54",
        "55-59",
        "60-64",
        "65-69",
        "70-74",
        "75-79",
        "80 y mas",
    ],
    [2, 7, 12, 17, 22, 27, 32, 37, 42, 47, 52, 57, 62, 67, 72, 77, 82],
)


# Corroboramos
data_encoded.head()

t_egreso = pd.get_dummies(data_encoded, columns=["tipo_egreso"])

# Corroboramos los tipos del dataset
t_egreso.dtypes

x = data_encoded["grupo_edad"]
y = t_egreso["tipo_egreso_Defuncion"]

X_train, X_test, y_train, y_test = train_test_split(
    x, y, test_size=0.3, random_state=42
)

# Crea una instancia del modelo de regresión logística
model_edad = LogisticRegression()

# Convierte la variable "grupo_edad" en una matriz 2D. Necesario para la libreria que usamos.
X_train_reshaped = X_train.values.reshape(-1, 1)

# Ajusta (entrena) el modelo utilizando los datos de entrenamiento
model_edad.fit(X_train_reshaped, y_train)
predicciones = model_edad.predict(X_train_reshaped)
print(predicciones.tolist())

# Nombre de archivo donde deseas guardar el modelo
modelo_filename = './Regresion_edad_def.pkl'

# Abre el archivo en modo binario para escritura
# with open(modelo_filename, 'wb') as modelo_file:
#     pickle.dump(model_edad, modelo_file)

# # Convertimos a matriz 2D
# X_test_reshaped = X_test.values.reshape(-1, 1)

# # Realiza predicciones en el conjunto de prueba
# y_pred = model_edad.predict(X_test_reshaped)

# # Calcula la precisión del modelo
# accuracy = accuracy_score(y_test, y_pred)
# print(f"Precisión del modelo: {accuracy:.2f}")

# # Convertimos a matriz 2D
# X_test_reshaped = X_test.values.reshape(-1, 1)

# # Obtiene las predicciones del modelo de Regresión Logística
# logreg_predictions = model_edad.predict_proba(X_test_reshaped)[:, 1]

# # Combina las predicciones con las características originales
# X_train_combined = np.column_stack((X_train, np.zeros(X_train.shape[0])))
# X_test_combined = np.column_stack((X_test_reshaped, logreg_predictions))

# # Entrena un modelo de Gradient Boosting (XGBoost) con las características combinadas
# xgb_model = XGBClassifier()
# xgb_model.fit(X_train_combined, y_train)

# # Evalúa el modelo de Gradient Boosting en el conjunto de prueba
# accuracy = xgb_model.score(X_test_combined, y_test)
# print(f"Accuracy del modelo de Gradient Boosting: {accuracy:.2f}")

# # Obtiene las predicciones de probabilidad del modelo de Gradient Boosting
# xgb_predictions = xgb_model.predict_proba(X_test_combined)[:, 1]

# # Calcula la curva ROC
# fpr, tpr, _ = roc_curve(y_test, xgb_predictions)
# roc_auc = auc(fpr, tpr)

# # Grafica la curva ROC para el modelo de Gradient Boosting
# plt.figure(figsize=(8, 6))
# plt.plot(fpr, tpr, color="darkorange", lw=2, label="ROC curve (area = %0.2f)" % roc_auc)
# plt.plot([0, 1], [0, 1], color="navy", lw=2, linestyle="--")
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.05])
# plt.xlabel("False Positive Rate")
# plt.ylabel("True Positive Rate")
# plt.title("Receiver Operating Characteristic - Gradient Boosting")
# plt.legend(loc="lower right")
# plt.show()

# # Calcula el Mean Squared Error (MSE) y el Mean Absolute Error (MAE)
# mse = mean_squared_error(y_test, xgb_predictions)
# mae = mean_absolute_error(y_test, xgb_predictions)

# print(f"Mean Squared Error (MSE): {mse:.2f}")
# print(f"Mean Absolute Error (MAE): {mae:.2f}")