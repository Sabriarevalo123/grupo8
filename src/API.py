from fastapi import FastAPI
import joblib
import pandas as pd
import matplotlib.pyplot as plt
from io import BytesIO
import base64
from typing import List

app = FastAPI()

# Cargar el modelo
modelo = joblib.load("./Regresion_edad_def.pkl")

@app.post("/predecir/")
def predecir_valores(edades: List[int]):
    # Crear un dataframe con los valores recibidos
    features = pd.DataFrame({'grupo_edad': edades})
    # Realizar la predicción
    prediccion = modelo.predict(features)

    # Crear una tabla con valores originales y predicciones
    data = {'Valores Originales': edades, 'Predicciones': prediccion}
    df = pd.DataFrame(data)

    # Crear una imagen de la tabla usando matplotlib
    plt.figure(figsize=(8, 6))
    ax = plt.gca()
    ax.axis('off')
    table = plt.table(cellText=df.values, colLabels=df.columns, loc='center')
    table.auto_set_font_size(False)
    table.set_fontsize(14)
    table.scale(1.2, 1.2)

    # Convertir la tabla en una imagen
    img_buffer = BytesIO()
    plt.savefig(img_buffer, format='png', bbox_inches='tight', pad_inches=0.2)
    img_buffer.seek(0)
    img_base64 = base64.b64encode(img_buffer.read()).decode()

    return {"imagen_tabla_base64": img_base64}